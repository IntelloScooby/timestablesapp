package com.example.mikitashah.timestables

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), SeekBar.OnSeekBarChangeListener {

    private val tableUpdatesController = TableUpdatesController()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        seekBar.setOnSeekBarChangeListener(this)
        updateTable()
    }

    private fun updateTable() {
        timesTable.adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, tableUpdatesController.getTableValues())
    }

    override fun onProgressChanged(slider: SeekBar?, progress: Int, fromUser: Boolean) {
        seekBar.progress = tableUpdatesController.onUpdateSeekBarProgress(progress)
        updateTable()
    }

    override fun onStartTrackingTouch(p0: SeekBar?) {
    }

    override fun onStopTrackingTouch(p0: SeekBar?) {
    }
}
