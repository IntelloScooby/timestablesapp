package com.example.mikitashah.timestables

class TableUpdatesController {

    private val MIN_VALUE = 1
    private var seekBarValue = 1

    fun onUpdateSeekBarProgress(progress: Int): Int {
        seekBarValue = if (progress < MIN_VALUE) {
            MIN_VALUE
        } else {
            progress
        }
        return seekBarValue
    }

    fun getTableValues(): Array<Int> {
        return Array(10, { i -> (seekBarValue * (i + 1)) })
    }
}